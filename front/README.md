## Piquante

Dossier contenant le code du frontend du projet 6 "Piquante" de la formation Développeur Web.  
Le projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

Pour faire fonctionner le projet, executer la commande `npm install`.

## Development server

Démarrer `ng serve` ou `npm start` pour avoir accès au serveur de développement. Rendez-vous sur `http://localhost:4200/`. L'application va se recharger automatiquement si vous modifiez un fichier source.
