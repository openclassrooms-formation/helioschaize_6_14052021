## Piquante

Dossier contenant le code du backend du projet 6 "Piquante" de la formation Développeur Web.

Pour faire fonctionner le projet, executer la commande `npm install` + `npm install -g nodemon`

## Base de données et Tokens

Pour que l'application fonctionne, créer un fichier nommé `.env` a la racine de ce dossier et coller dedans les informations transmises par mail.

## Development server

Démarrer `nodemon server` pour lancer l'API. L'API est lancée sur le port `3000`. L'API va se recharger automatiquement si vous modifiez un fichier source.
