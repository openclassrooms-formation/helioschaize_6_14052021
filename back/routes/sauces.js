const express = require('express');
const router = express.Router();

const sauceCtrl = require('../controllers/sauces');
const authVerif = require('../middlewares/auth');
const multer = require('../middlewares/multer-config');

router.get("/", authVerif, sauceCtrl.getAllSauces);
router.get("/:id", authVerif, sauceCtrl.getOneSauce);
router.post("/", authVerif, multer, sauceCtrl.createSauce);
router.delete("/:id", authVerif, sauceCtrl.deleteSauce);
router.put("/:id", authVerif, multer, sauceCtrl.modifySauce);
router.post("/:id/like", authVerif, sauceCtrl.likeSauce);

module.exports = router;