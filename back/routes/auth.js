const express = require('express');
const router = express.Router();
const expressBouncer = require('express-bouncer')(5000,600000,3);

const authCtrl = require('../controllers/auth');

router.post('/signup', authCtrl.signup);
router.post('/login', expressBouncer.block, authCtrl.login);

module.exports = router;