## Piquante

Repository du projet 6 "Piquante" de la formation Développeur Web d'OpenClassrooms.

## BACK

Dossier `back` contenant le code de l'API pour le projet, instruction de lancement directement dedans.

## FRONT
Dossier `front` contenant le code du front-end pour le projet, instruction de lancement directement dedans.
